# Evidence oprav

This is sample application for evidence of orders created for course [Webové technologie](https://plus4u.net/ues/sesm?action=ues:UCL-BT:WEB14A16W.CZ/FULLTIME).


## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

If you wish, populate db with sample data:

```
$ rails db:seed
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```