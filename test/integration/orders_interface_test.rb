require 'test_helper'

class OrdersInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:inza)
  end

  test 'order interface' do
    log_in_as(@user)
    get root_path
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    # Invalid submission
    assert_no_difference 'Order.count' do
      post orders_path, params: { order: { description: ''} }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    content = 'Ema má mísu'
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
    assert_difference 'Order.count', 1 do
      post orders_path, params: { order: { description: content, picture: picture } }
    end
    assert assigns(:order).picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    # Delete post
    assert_select 'a', text: 'delete'
    first_order = @user.orders.paginate(page: 1).first
    assert_difference 'Order.count', -1 do
      delete order_path(first_order)
    end
    # Visit different user (no delete links)
    get user_path(users(:martin))
    assert_select 'a', text: 'delete', count: 0
  end

  test 'order sidebar count' do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.orders.count} orders", response.body
    # User with zero microposts
    other_user = users(:standa)
    log_in_as(other_user)
    get root_path
    assert_match '0 orders', response.body
    other_user.orders.create!(description: 'An order')
    get root_path
    assert_match '1 order', response.body
  end
end
