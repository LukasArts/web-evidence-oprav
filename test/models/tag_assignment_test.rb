require 'test_helper'

class TagAssignmentTest < ActiveSupport::TestCase
  def setup
    @tag_assignment = TagAssignment.new(tagged_order_id: orders(:naramek).id,
                                        tag_id: tags(:express).id)
  end

  test 'should be valid' do
    assert @tag_assignment.valid?
  end

  test 'tag should be applied at most once per order' do
    assert_raises(ActiveRecord::RecordNotUnique) do
      @tag_assignment.save
      duplicate_tag_assignment = @tag_assignment.dup
      duplicate_tag_assignment.save
    end
  end
end
