require 'test_helper'

class OrderTest < ActiveSupport::TestCase

  def setup
    @user = users(:inza)
    @order = @user.orders.build(description: 'zetor')
  end

  test 'should be valid' do
    assert @order.valid?
  end

  test 'user id should be present' do
    @order.user_id = nil
    assert_not @order.valid?
  end

  test 'content should be present' do
    @order.description = '   '
    assert_not @order.valid?
  end

  test 'content should be at most 300 characters' do
    @order.description = 'a' * 301
    assert_not @order.valid?
  end

  test 'order should be most recent first' do
    assert_equal orders(:most_recent), Order.first
  end
end
