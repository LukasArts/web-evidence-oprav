require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test 'should get home' do
    get root_path
    assert_response :success
    assert_select 'title', 'Evidence oprav'
  end

  test 'should get help' do
    get help_path
    assert_response :success
    assert_select 'title', 'Nápověda | Evidence oprav'
  end

  test 'should get repairs' do
    get repairs_url
    assert_response :success
    assert_select 'title', 'Zakázky | Evidence oprav'
  end

  test 'should get customers' do
    get customers_url
    assert_response :success
    assert_select 'title', 'Zákazníci | Evidence oprav'
  end

  test 'should get about' do
    get about_url
    assert_response :success
    assert_select 'title', 'O tomto programu | Evidence oprav'
  end

  test 'should get contact' do
    get contact_url
    assert_response :success
    assert_select 'title', 'Kontakty | Evidence oprav'
  end

  test 'shoud get signup' do
    get signup_url
    assert_response :success
    assert_select 'title', 'Registrace | Evidence oprav'
  end

end