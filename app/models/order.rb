class Order < ApplicationRecord
  belongs_to :user, inverse_of: :orders
  has_many :tag_assignments, foreign_key: 'tagged_order_id'
  has_many :tags, through: :tag_assignments
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :description, presence: true, length: {maximum: 300}
  validate :picture_size


  def tag(tag)
    tags << tag
  end

  def untag(tag)
    tags.delete(tag)
  end

  def tagged_with?(tag)
    tags.include?(tag)
  end


  private

    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, 'should be less than 5MB')
      end
    end
end
