class Tag < ApplicationRecord
  has_many :tag_assignments
  has_many :orders, through: :tag_assignments
  validates :name, presence: true, length: {maximum: 20}
end
