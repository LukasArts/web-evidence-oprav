class TagAssignment < ApplicationRecord
  belongs_to :tagged_order, class_name: 'Order'
  belongs_to :tag
  validates :tagged_order_id, presence: true
  validates :tag_id, presence: true
  validate :validate_tagged_order_id
  validate :validate_tag_id

  private

  def validate_tagged_order_id
    errors.add(:tagged_order_id, 'not present') unless Order.exists?(self.tagged_order_id)
  end

  def validate_tag_id
    errors.add(:tag_id, 'not present') unless Tag.exists?(self.tag_id)
  end
end
