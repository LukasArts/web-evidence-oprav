class OrdersController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :edit, :update]
  before_action :correct_user, only: [:destroy, :edit, :update]

  def create
    tags = {}
    tags[:tag_ids] = order_params.delete('tag_ids')
    reduced_params = order_params.reject { |k, _| k == 'tag_ids' }
    @order = current_user.orders.build(reduced_params)
    @order.save
    @order.update_attributes(tags)
    if @order.save
      flash[:success] = 'Order saved!'
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def edit
    @feed_items = []
    render 'static_pages/home'
  end

  def update
    if @order.update_attributes(order_params)
      flash[:success] = 'Order updated'
      redirect_to user_url(current_user)
    else
      render 'edit'
    end
  end

  def destroy
    @order.destroy
    flash[:success] = 'Order deleted'
    redirect_back(fallback_location: root_url)
  end


  private

    def order_params
      params.require(:order).permit(:description, :picture, tag_ids: [])
    end

    def correct_user
      @order = current_user.orders.find_by(id: params[:id])
      redirect_to root_url if @order.nil?
    end
end
