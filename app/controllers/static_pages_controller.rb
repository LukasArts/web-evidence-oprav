class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @order = current_user.orders.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @tags = Tag.all
    end
  end

  def help
  end

  def repairs
  end

  def customers
  end

  def about
  end

  def contact
  end

end
