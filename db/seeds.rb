# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create!(name: 'Inža',
             email: 'tomas@jukin.cz',
             password: 'password',
             password_confirmation: 'password',
             admin: true)

User.create!(name: 'Martin',
             email: 'svobom57@gmail.com',
             password: 'password',
             password_confirmation: 'password')

User.create!(name: 'Lukáš',
             email: 'kinodont@gmail.com',
             password: 'password',
             password_confirmation: 'password')

User.create!(name: 'Vojta',
             email: 'v.samla@centrum.cz',
             password: 'password',
             password_confirmation: 'password')

User.create!(name: 'Cyril',
             email: 'cyrilcermak@gmail.com',
             password: 'password',
             password_confirmation: 'password')

95.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@unicorn.com"
  password = 'password'
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
50.times do
  description = Faker::Lorem.sentence(5)
  users.each { |user| user.orders.create!(description: description) }
end

10.times do
  name = (Faker::Ancient.god)
  Tag.create!(name: name) unless Tag.find_by(name: name)
end
users = User.order(:created_at).take(6)
users.each do |u|
  u.orders.order(:created_at)[1..10].each do |o|
    rand(4).times do
      tag = Tag.find(rand(1..Tag.all.size))
      o.tags << tag unless o.tags.find_by_name(tag.name)
    end
  end
end