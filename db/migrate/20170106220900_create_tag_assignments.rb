class CreateTagAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :tag_assignments do |t|
      t.integer :tagged_order_id, index: true, foreign_key: true
      t.integer :tag_id, index: true, foreign_key: true

      t.timestamps
    end
    add_index :tag_assignments, [:tagged_order_id, :tag_id], unique: true
  end
end
